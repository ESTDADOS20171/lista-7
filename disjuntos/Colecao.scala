package disjuntos

/**
 * O 2o param, f :E=> e uma funcao que mapeaia o valor qualquer em um valor inteiro unico.
 */

class Colecao[E <% Ordered[E] : Manifest](n: Int, f: E => Int) {

  private var R = Array.fill[Int](n)(-1); // representantes
  private var E = new Array[E](n); // valores

  def MakeSet(x: E): Int = {
    var rep = f(x);
    R(rep) = -1;
    E(rep) = x;
    rep
  }

  def Find(x: E): Int = {
    var pos = E.indexOf(x);

    if (pos != -1) {
      if (R(pos) == -1) {
        pos
      } else {
        Find(E(R(pos)))
      }
    } else {
      MakeSet(x)
    }
  }
  
  def MesmoConjunto(x: E, y: E) = Find(x) == Find(y);

  def Union(x: E, y: E) = R(Find(x)) = Find(y);

  override def toString(): String = {

    // identificar os indices de R com valor -1, ou seja, os representantes.
    var reps = R.zipWithIndex.filter(_._1 == -1).map(_._2);

    // para cada indice de representante, buscar os elementos e montar a string
    // do toString {# Representante => x,y,z (Elementos)}
    reps.sorted.map(x =>
      "{#" + E(x) + " => " + E.filter { y => Find(y) == E(x) }.mkString(", ") + "}").mkString(", ")
  }
  
  def liberar() = {
    E = null
    R = null
  }

}