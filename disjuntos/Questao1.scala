package disjuntos

object Questao1 extends App {

  var c = new Colecao[Int](10, x => x);

  for (i <- 0 until 10) {
    c.MakeSet(i);
  }

  println("Colecao de conjuntos disjuntos.")
  println(c)
  
  println("\nUniao entre 2 e 3")
  c.Union(2, 3);
  println(c)
  
  println("\n2 e 3 pertencem ao mesmo conjunto? " + c.MesmoConjunto(2, 3));
  
  println("2 e 4 pertencem ao mesmo conjunto? " + c.MesmoConjunto(2, 4));


 println("\nUniao entre 2 e 1")
  c.Union(2, 1);

  println(c)

  println("\nUniao entre 4 e 5")
  c.Union(4, 5);

  println(c)

  println("\nUniao entre 6 e 7")
  c.Union(6, 7);

  println(c)

  println("\nUniao entre 8 e 9")
  c.Union(8, 9);

  println(c)

  println("\nUniao entre 3 e 6")
  c.Union(3, 6);

  println(c)

  println("\nUniao entre 4 e 0")
  c.Union(0, 4);

  println(c)
  
  println("\nQuem e o representante do 4? " + c.Find(4));
  
  println("Quem e o representante do 1? " + c.Find(1));
  
  println("Quem e o representante do 7? " + c.Find(7));

   println("\nUniao entre 5 e 8")
  c.Union(5, 8);

  println(c)

  println("\nUniao entre 1 e 0")
  c.Union(1, 0);

  println(c)
}